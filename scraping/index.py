# import requests
# import os

# files = [
#     'https://www.freemovies360.com/sitemap-list-1.xml',
#     'https://www.freemovies360.com/sitemap-list-2.xml',
#     'https://www.freemovies360.com/sitemap-list-3.xml',
#     'https://www.freemovies360.com/sitemap-list-4.xml',
#     'https://www.freemovies360.com/sitemap-list-5.xml',
#     'https://www.freemovies360.com/sitemap-list-6.xml',
#     'https://www.freemovies360.com/sitemap-list-7.xml',
#     'https://www.freemovies360.com/sitemap-list-8.xml',
#     'https://www.freemovies360.com/sitemap-list-9.xml',
#     'https://www.freemovies360.com/sitemap-list-10.xml',
#     'https://www.freemovies360.com/sitemap-list-11.xml',
#     'https://www.freemovies360.com/sitemap-list-12.xml',
#     'https://www.freemovies360.com/sitemap-list-13.xml',
#     'https://www.freemovies360.com/sitemap-list-14.xml',
#     'https://www.freemovies360.com/sitemap-list-15.xml',
#     'https://www.freemovies360.com/sitemap-list-16.xml',
#     'https://www.freemovies360.com/sitemap-list-17.xml',
#     'https://www.freemovies360.com/sitemap-list-18.xml',
#     'https://www.freemovies360.com/sitemap-list-19.xml',
#     'https://www.freemovies360.com/sitemap-list-20.xml',
#     'https://www.freemovies360.com/sitemap-list-21.xml',
#     'https://www.freemovies360.com/sitemap-list-22.xml',
#     'https://www.freemovies360.com/sitemap-list-23.xml',
#     'https://www.freemovies360.com/sitemap-list-24.xml',
# ]

# if os.path.exists('sitemap'):

#     for link in files :

#         linkUrl = link.split('/')

#         fileName = linkUrl[len(linkUrl) - 1]

#         response = requests.get(link)

#         path = 'sitemap/' + fileName

#         if os.path.exists(path):
#             os.remove(path)

#         open(path , 'wb').write(response.content)

#         print('file installed : ' + link)

# # from bs4 import BeautifulSoup
# # import bs4

# # sitemapFile = open('sitemap/sitemap.xml' , 'r')

# # data = BeautifulSoup(sitemapFile.read() , 'xml')

# # for i in data.find_all('loc'):
# #     print(i.get_text())




from bs4 import BeautifulSoup
from Classes.SitemapCloner import SitemapCloner

basePath = "sitemaps"

is_fetched = SitemapCloner('https://www.freemovies360.com/').fetch_sitemap(basePath)

if is_fetched:
    urls = []
    data = BeautifulSoup(open(basePath + '/sitemap.xml' , 'r').read() , 'xml')
    for link in data.find_all('loc'):
        SitemapCloner(''  , link.get_text()).fetch_sitemap(basePath)
        print(link.get_text())

