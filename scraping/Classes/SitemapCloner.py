import os
import requests


class SitemapCloner:

    sitemapURL = ""

    def __init__(self, domain, sitemapPath=False):
        if sitemapPath:
            self.sitemapURL = sitemapPath
        else:
            self.sitemapURL = domain + '/sitemap.xml'

    def check_sitemap_file(self):
        response = requests.get(self.sitemapURL)

        return response.status_code == 200

    def fetch_sitemap(self, baseBath="sitemap"):
        if self.check_sitemap_file():
            if (os.path.exists(baseBath) != True):
                os.mkdir(baseBath)

            response = requests.get(self.sitemapURL)

            fileName = self.sitemapURL.split(
                '/')[len(self.sitemapURL.split('/')) - 1]

            filepath = baseBath + '/' + fileName

            open(filepath, 'wb').write(response.content)

            print('file stored on : ',filepath)

            return True

        else:
            print(
                'file does not exists in host, please veridy this file : ' + self.sitemapURL)

            return False
